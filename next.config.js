module.exports = {
  output: "export",
  distDir: "out",
  trailingSlash: true,
  pageExtensions: ["js", "jsx"],
};
