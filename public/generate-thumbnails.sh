#!/usr/bin/env bash

set -e

echo "Removing old thumbnails"
rm -rf thumbnails/

echo "Copying images"
cp -r images/ thumbnails/

echo "Processing images"
find thumbnails/ -type f -iname "*.jpg" -print -exec \
  mogrify -resize "800^>" -gravity center -crop 800x800+0+0 -strip -profile /usr/share/color/icc/sRGB.icc -quality 75 -define jpeg:dct-method=float {} \;
