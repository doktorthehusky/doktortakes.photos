import styles from "./Sidebar.module.scss";
import Link from "next/link";
import Categories from "./Categories";
import { useState } from "react";
import MenuIcon from "./MenuIcon";
import SidebarList from "./SidebarList";

export default function Sidebar(props) {
  const [menuOpen, setMenuOpen] = useState(false);

  function closeMenu() {
    setMenuOpen(false);
  }

  function openMenu() {
    setMenuOpen(true);
  }

  return (
    <nav className={styles.Sidebar}>
      <header className={styles.SidebarHeader}>
        <h1 className={styles.Logo}>
          <Link href="/" className={styles.LogoBlue}>
            <div>Doktor</div>
            <div>Takes</div>
            <div>Photos</div>
          </Link>
        </h1>
      </header>

      <button
        className={[
          styles.SidebarMenuButton,
          styles.SidebarOpenMenuButton,
        ].join(" ")}
        onClick={openMenu}
      >
        <MenuIcon className={styles.SidebarMenuIcon} />
        <span>Menu</span>
      </button>

      <div
        className={[styles.SidebarMenu, menuOpen ? styles.Open : ""].join(" ")}
      >
        <button
          type="button"
          className={[
            styles.SidebarMenuButton,
            styles.SidebarCloseMenuButton,
          ].join(" ")}
          onClick={closeMenu}
        >
          &times; close
        </button>

        <section className={styles.SidebarSection}>
          <h2>
            <Link href={"/about"}>About</Link>
          </h2>
        </section>

        <section className={styles.SidebarSection}>
          <h2>Photos</h2>
          <Categories categories={props.categories} onClick={closeMenu} />
        </section>

        <section className={styles.SidebarSection}>
          <h2>
            <a href="https://gallery.doktortakes.photos">Full gallery</a>
          </h2>
        </section>

        <section className={styles.SidebarSection}>
          <h2>
            <a
              href="https://docs.google.com/document/d/1iWG2WM-nicocfP_vT6-VF8KO-EPla8-AARo5x7rZXIs/edit?usp=sharing"
              rel="noreferrer noopener nofollow"
            >
              Commissions
            </a>
          </h2>
        </section>

        <section className={styles.SidebarSection}>
          <h2>Links</h2>
          <SidebarList>
            <a href="https://bsky.app/profile/bluedog.zone">Bluesky</a>
            <a href="mailto:doktorthehusky@gmail.com">Email</a>
            <a href="https://www.furtrack.com/index/photographer:doktor">
              FurTrack
            </a>
            <a href="https://t.me/DoktorTakesPhotos">Telegram</a>
            <a href="https://twitter.com/DoktorTheHusky">Twitter</a>
            <a href="https://doktorthehusky.com">Website</a>
          </SidebarList>
        </section>

        <footer className={styles.SidebarFooter}>
          <div>&copy; Doktor</div>
          <div>last updated 2024-04-30</div>
        </footer>
      </div>
    </nav>
  );
}
