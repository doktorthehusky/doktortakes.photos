import styles from "./Gallery.module.scss";

export default function Gallery(props) {
  return (
    <section className={styles.Gallery}>
      {props.parent && (
        <h2 className={styles.GalleryTitle}>
          <div className={styles.GalleryTitleParent}>{props.parent.name}</div>
          <div className={styles.GalleryTitleSeparator}>&nbsp;//&nbsp;</div>
          <div className={styles.GalleryTitleChild}>{props.child.name}</div>
        </h2>
      )}

      <div className={styles.GalleryItems}>
        {props.photos.map((filename) => (
          <div className={styles.GalleryItem} key={filename}>
            <a href={filename}>
              <img className={styles.GalleryImage} src={filename.replace("images", "thumbnails")} alt="" />
            </a>
          </div>
        ))}
      </div>
    </section>
  );
}
