import Link from "next/link";
import styles from "./Category.module.scss";
import { useRouter } from "next/router";

export default function Category({ category, subcategory, onClick }) {
  const router = useRouter();
  const currentHref = router.asPath;

  const href = "/" + category.slug + "/" + subcategory.slug + "/";

  return (
    <Link
      href={href}
      className={[currentHref === href ? styles.CategoryActive : ""].join(" ")}
      onClick={onClick}
    >
      {subcategory.name}
    </Link>
  );
}
