export default function MenuIcon(props) {
  return (
    <svg className={props.className} width="10" height="10" viewBox="0 0 10 10">
      <path d="M0,1 10,1" stroke="#000" strokeWidth="2" />
      <path d="M0,5 10,5" stroke="#000" strokeWidth="2" />
      <path d="M0,9 10,9" stroke="#000" strokeWidth="2" />
    </svg>
  );
}
