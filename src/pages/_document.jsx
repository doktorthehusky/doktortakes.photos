import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html>
      <Head>
        <meta charSet="utf-8" />
        <meta name="author" content="Doktor" />
        <meta name="description" content="Photography by Doktor." />
        <meta name="theme-color" content="#0078ff" />

        <meta property="og:title" content="Doktor Takes Photos" />
        <meta property="og:description" content="Photography by Doktor." />
        <meta property="og:url" content="https://doktortakes.photos" />
        <meta property="og:locale" content="en_US" />

        <meta property="og:image" content="/blue-dog-512.png" />
        <meta property="og:image:type" content="image/png" />
        <meta property="og:image:width" content="512" />
        <meta property="og:image:height" content="512" />

        <meta property="og:type" content="profile" />
        <meta property="profile:first_name" content="Doktor" />
        <meta property="profile:username" content="Doktor" />

        <link rel="icon" type="image/png" sizes="256x256" href="/blue-dog-256.png" />

        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="anonymous" />
        <link
          href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,200;0,400;0,700;0,900&display=swap"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=JetBrains+Mono:ital,wght@0,800&display=swap"
          rel="stylesheet"
        />

        <script defer data-domain="doktortakes.photos" src="https://plausible.io/js/script.js"></script>
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
